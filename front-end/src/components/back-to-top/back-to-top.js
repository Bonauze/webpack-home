const moduleName = `backToTop`;

export default (() => {
    setTimeout(() => {
        Array.from(document.querySelectorAll(`.js-back-to-top`)).forEach((wrapper) => {
            if (wrapper.dataset[`${moduleName}Init`] === `true`) return;
            wrapper.dataset[`${moduleName}Init`] = `true`;



            let lastTimeout;



            showOrHideBtn();



            window.addEventListener(`scroll`, showOrHideBtn);

            wrapper.addEventListener(`click`, (event) => {
                event.preventDefault();

                $(`html, body`).animate({
                    scrollTop: document.documentElement.offsetTop
                }, 800);
            });



            function showOrHideBtn() {
                clearTimeout(lastTimeout);

                lastTimeout = setTimeout(() => {
                    if ((document.documentElement.scrollTop || document.body.scrollTop) > window.innerHeight) {
                        wrapper.classList.add(`visible`);
                    } else {
                        wrapper.classList.remove(`visible`);
                    }
                }, 200);
            }



        });
    }, 0);
})();