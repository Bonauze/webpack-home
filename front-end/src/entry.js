import 'core-js';

import './js/polyfills';

import './js/modules/prerender';
import './js/modules/pop-up';
import './js/modules/input-mask';
import './js/modules/scroll-to-anchor';
import './js/modules/yandex-map';
import './js/modules/fancybox';
import './js/modules/slick-сarousel';
import './js/modules/lazy-load';

import './components/back-to-top/back-to-top';

import './styles-entry';