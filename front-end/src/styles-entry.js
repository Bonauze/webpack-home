import 'normalize.css';

import './sass/base/_variables.scss';
import './sass/base/base.scss';
import './sass/base/fonts.scss';

import './sass/modules/pop-up.scss';
import './sass/modules/page.scss';

import './components/back-to-top/back-to-top.scss';