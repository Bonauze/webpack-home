import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick.min.js';

const moduleName = `slickCarousel`;

export default (() => {
	setTimeout(() => {
		Array.from(document.querySelectorAll(`.js-slider`)).forEach((wrapper) => {
			if (wrapper.dataset[`${moduleName}Init`] === `true`) return;
			wrapper.dataset[`${moduleName}Init`] = `true`;



			const list = wrapper.querySelector(`.js-slider-list`);



			if (list.children.length === 1) return;



			$(list).slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: true,
				infinite: true,
				autoplaySpeed: 5000,
				speed: 600,
				arrows: true,
				dots: true,
				prevArrow: `<button type="button" class="slider__btn slider__btn--prev"></button>`,
				nextArrow: `<button type="button" class="slider__btn slider__btn--next"></button>`
			});



		});
	}, 0);
})();