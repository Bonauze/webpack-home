const moduleName = `prerender`;

export default (() => {
    setTimeout(() => {
        Array.from(document.querySelectorAll(`a:not([data-prerender="false"])`)).forEach((wrapper) => {
            if (wrapper.dataset[`${moduleName}Init`] === `true`) return;
            wrapper.dataset[`${moduleName}Init`] = `true`;



            let lastTimeout;



            wrapper.addEventListener(`mouseover`, (event) => {
                clearTimeout(lastTimeout);

                lastTimeout = setTimeout(() => {
                    const target = event.target.closest(`a`);
                    const href   = target.href;

                    if (!href || target.getAttribute(`href`)[0] === `#` || href.indexOf(location.host) === -1) return;

                    const element = document.createElement(`link`);
                    element.setAttribute(`rel`, `prerender`);
                    element.setAttribute(`href`, href);
                    document.querySelector(`head`).appendChild(element);
                }, 100);
            }, {
                once: true,
                passive: true
            });



        });
    }, 0);
})();