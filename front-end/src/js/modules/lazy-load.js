import inViewport from 'in-viewport';

const moduleName = `lazyLoad`;

export default (() => {
    setTimeout(() => {
        Array.from(document.querySelectorAll(`.js-lazy-load`)).forEach((wrapper) => {
            if (wrapper.dataset[`${moduleName}Init`] === `true`) return;
            wrapper.dataset[`${moduleName}Init`] = `true`;



            const events  = [`load`, `scroll`, `resize`, `lazyLoad`];



            if (isVisible()) {
                uploadImage();
            } else {
                registerEventHandlers();
            }



            function isVisible() {
                return wrapper.offsetParent !== null && inViewport(wrapper, {
                    offset: 300
                });
            }



            function uploadImage() {
                const image = new Image();
                const src   = getSrc();

                image.onload = () => showImage(src);
                image.onerror = () => setTimeout(uploadImage, 4000);

                image.src = src;
            }



            function getSrc() {
                const modernSrc   = wrapper.dataset[`modernSrc`];
                const outdatedSrc = wrapper.dataset[`outdatedSrc`];

                if (modernSrc && document.documentElement.classList.contains(`webp`)) {
                    return modernSrc;
                } else {
                    return outdatedSrc;
                }
            }



            function tryUploadImage() {
                if (isVisible()) {
                    removeEventHandlers();
                    uploadImage();
                }
            }



            function registerEventHandlers() {
                events.forEach((event) => window.addEventListener(event, tryUploadImage));
            }



            function removeEventHandlers() {
                events.forEach((event) => window.removeEventListener(event, tryUploadImage));
            }



            function showImage(src) {
                wrapper.removeAttribute(`data-modern-src`);
                wrapper.removeAttribute(`data-outdated-src`);

                if (navigator.userAgent.indexOf(`MSIE`) !== -1 || navigator.appVersion.indexOf(`Trident/`) > 0) {
                    wrapper.setAttribute(`src`, src);
                } else {
                    wrapper.style.opacity = `0`;
                    wrapper.setAttribute(`src`, src);

                    setTimeout(() => { wrapper.style.transition = `0.8s opacity`; }, 100);
                    setTimeout(() => { wrapper.style.opacity = 1; }, 200);
                    setTimeout(() => { wrapper.removeAttribute(`style`); }, 2000);
                }
            }



        });
    }, 0);
})();