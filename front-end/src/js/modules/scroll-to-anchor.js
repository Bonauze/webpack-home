const moduleName = `scrollToAnchor`;

export default (() => {
    setTimeout(() => {
        Array.from(document.querySelectorAll(`.js-scroll-to-anchor`)).forEach((wrapper) => {
            if (wrapper.dataset[`${moduleName}Init`] === `true`) return;
            wrapper.dataset[`${moduleName}Init`] = `true`;



            wrapper.addEventListener(`click`, (event) => {
                event.preventDefault();

                $(`html, body`).animate({
                    scrollTop: document.querySelector(wrapper.getAttribute(`href`)).offsetTop - document.querySelector(`#topbar`).offsetHeight
                }, 800);
            });



        });
    }, 0);
})();