const moduleName = `popUp`;

export default (() => {
    Array.from(document.querySelectorAll(`.js-pop-up`)).forEach((wrapper) => {
        if (wrapper.dataset[`${moduleName}Init`] === `true`) return;
        wrapper.dataset[`${moduleName}Init`] = `true`;



        const popUp = wrapper;



        popUp.addEventListener(`click`, (event) => {
            const btnClose = event.target.closest(`.js-pop-up-btn-close`);
            if (!btnClose) return;

            event.preventDefault();
            event.stopPropagation();

            hidePopUp(popUp);
        });



        popUp.addEventListener(`click`, (event) => {
            const insides = event.target.closest(`.js-pop-up-insides`);
            if (insides) return;

            event.stopPropagation();

            hidePopUp(popUp);
        });



        new MutationObserver(() => {
            putFocusOnTheFirstInput(popUp);
            centeringPopUp(popUp);
            callOtherModules();

            popUp.classList.remove(`loading`);
        }).observe(popUp, {
            childList: true,
            characterData: true,
            subtree: true
        });
    });



    Array.from(document.querySelectorAll(`.js-pop-up-btn-open`)).forEach((wrapper) => {
        if (wrapper.dataset[`${moduleName}Init`] === `true`) return;
        wrapper.dataset[`${moduleName}Init`] = `true`;



        const popUpBtnOpen = wrapper;



        popUpBtnOpen.addEventListener(`click`, (event) => {
            const btnOpen = event.target.closest(`.js-pop-up-btn-open`);
            if (!btnOpen) return;

            event.preventDefault();

            const popUpId = btnOpen.dataset[`popUpId`];
            const popUp   = document.querySelector(`#${popUpId}`);

            showPopUp(popUp, popUpBtnOpen);
        });
    });
})();



const windowData = {
    windowScrollTop: null,
    htmlHeight: null
};

const body = document.querySelector(`body`);

const activePopUp = [];









window.addEventListener(`resize`, () => {
    if (activePopUp.length) {
        centeringPopUp(activePopUp[activePopUp.length -1]);
    }
});



document.addEventListener(`keydown`, (event) => {
    if (event.keyCode === 27 && activePopUp.length) {
        event.preventDefault();

        hidePopUp((activePopUp[activePopUp.length -1]));
    }
});









window.showPopUp = (popUpId) => {
    const popUp = document.querySelector(`#${popUpId}`);

    if (!(popUp.classList.contains(`active`))) {
        showPopUp(popUp);
    }
};



function showPopUp(popUp, popUpBtnOpen) {
    if (!activePopUp.length) {
        updateWindowData();
    }

    const url        = popUp.dataset[`url`];
    const hasOnclick = !!popUpBtnOpen ? popUpBtnOpen.getAttribute(`onclick`) : undefined;

    if (url) {
        getPopUpByAjaxAndInsert(popUp, url);
    }

    if (hasOnclick) {
        popUp.innerHTML = ``;
        popUp.classList.add(`loading`);
    }

    popUp.classList.add(`active`);

    if (!url && !hasOnclick) {
        putFocusOnTheFirstInput(popUp);
    }

    if (hasVerticalScroll() && !activePopUp.length) {
        body.style.overflowY = `scroll`;
    }

    if (!activePopUp.length) {
        createAndAddStylesToDom();
    }

    if (!url && !hasOnclick) {
        centeringPopUp(popUp);
    }

    activePopUp.push(popUp);
}



function hidePopUp(popUp) {
    popUp.classList.remove(`active`);

    if (activePopUp.length === 1) {
        body.style.overflowY = `auto`;

        document.body.classList.remove(`styles-for-pop-up`);

        window.scrollBy(0, windowData.windowScrollTop);
    }

    activePopUp.splice(activePopUp.indexOf(popUp), 1);
}



function createAndAddStylesToDom() {
    const styles = `
        <style id="styles-for-pop-up">
            .styles-for-pop-up {
                position: fixed;
                top:      -${windowData.windowScrollTop}px;
                width:    100%;
                height:   ${windowData.htmlHeight}px;
            }
        </style>
    `;

    if (document.querySelector(`#styles-for-pop-up`)) {
        document.querySelector(`#styles-for-pop-up`).remove();
    }

    document.body.insertAdjacentHTML(`beforeend`, styles);
    document.body.classList.add(`styles-for-pop-up`);
}



function getPopUpByAjaxAndInsert(popUp, url) {
    const formData = new FormData();
    const xhr      = new XMLHttpRequest();

    formData.append(`id`, popUp.getAttribute(`id`));

    popUp.innerHTML = ``;
    popUp.classList.add(`loading`);

    xhr.open(`POST`, url);
    xhr.setRequestHeader(`X-Requested-With`, `XMLHttpRequest`);
    xhr.onreadystatechange = () => {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            popUp.innerHTML = xhr.responseText;

            putFocusOnTheFirstInput(popUp);
            centeringPopUp(popUp);

            popUp.classList.remove(`loading`);

            callOtherModules();
        }
    };
    xhr.send(formData);
}



function putFocusOnTheFirstInput(popUp) {
    if (popUp.dataset[`popUpPutFocus`] === `false`) return;

    setTimeout(() => {
        if (!(document.body.classList.contains(`mobile`))) {
            Array.from(popUp.querySelectorAll(`input, textarea`)).every((input) => {
                if (input.offsetParent !== null && input.value === ``) {
                    input.focus();
                    return false;
                } else {
                    return true;
                }
            });
        }
    }, 200);
}



function hasVerticalScroll() {
    return window.innerWidth > document.documentElement.clientWidth;
}



function centeringPopUp(popUp) {
    if (!popUp.querySelector(`.js-pop-up-insides`)) return;

    centeringPopUpAfterLoadingImages(popUp);

    const popUpInsides       = popUp.querySelector(`.js-pop-up-insides`);
    const popUpInsidesHeight = popUpInsides.offsetHeight;
    const windowHeight       = window.innerHeight;

    if (windowHeight - 120 > popUpInsidesHeight) {
        popUpInsides.style.marginTop = `${(windowHeight - popUpInsidesHeight) / 2}px`;
    } else {
        popUpInsides.removeAttribute(`style`);
    }
}



function centeringPopUpAfterLoadingImages(popUp) {
    const notUploadedImagesInPopUp = Array.from(popUp.querySelectorAll(`img`)).filter(image => !image.complete);

    if (!notUploadedImagesInPopUp.length) return;

    Array.from(notUploadedImagesInPopUp).forEach((imageInPopUp) => {
        const image = new Image();

        image.onload = () => {
            centeringPopUp(popUp);
        };

        image.src = imageInPopUp.getAttribute(`src`);
    });
}



function updateWindowData() {
    windowData.windowScrollTop = (document.documentElement.scrollTop || document.body.scrollTop);
    windowData.htmlHeight 	   = document.querySelector(`html`).offsetHeight;
}



function callOtherModules() {

}