import '@fancyapps/fancybox/dist/jquery.fancybox.min.css';
import '@fancyapps/fancybox/dist/jquery.fancybox.min.js';

const moduleName = `fancybox`;

export default (() => {
    setTimeout(() => {
        Array.from(document.querySelectorAll(`.js-fancybox`)).forEach((wrapper) => {
            if (wrapper.dataset[`${moduleName}Init`] === `true`) return;
            wrapper.dataset[`${moduleName}Init`] = `true`;



            const $elements       = $(wrapper).find(`[data-fancybox]`);
            const randomGroupName = Math.random().toString();



            $elements.each((index, $element) => {
                if (!($element.dataset[`fancybox`])) {
                    $element.dataset[`fancybox`] = randomGroupName;
                }
            });



            $elements.fancybox({
                loop: true,
                animationEffect: `fade`,
                transitionEffect: `slide`,
                buttons: [`close`]
            });



        });
    }, 0);
})();