import inViewport from 'in-viewport';
import scriptjs from 'scriptjs';

const moduleName = `yandexMap`;

export default (() => {
    setTimeout(() => {
        Array.from(document.querySelectorAll(`.js-yandex-map`)).forEach((wrapper) => {
            if (wrapper.dataset[`${moduleName}Init`] === `true`) return;
            wrapper.dataset[`${moduleName}Init`] = `true`;



            const jsonContainer  		 = wrapper.querySelector(`.js-yandex-map-json-container`);
            const buttonsOpeningBalloons = wrapper.querySelectorAll(`.js-yandex-map-btn-open-balloon`);
            const settings   	 		 = JSON.parse(jsonContainer.innerHTML);
            const scriptLang 	 		 = settings.hasOwnProperty(`lang`) ? settings.lang : `ru_RU`;

            let	map;
            let loadStarted = false;
            let ymapsLoaded = false;

            let windowWidth = window.innerWidth;



            if (!(inViewport(wrapper, { offset: 300 }))) {
                window.addEventListener(`scroll`, init);
                window.addEventListener(`resize`, init);
            } else {
                init();
            }






            window.addEventListener(`resize`, () => {
                if (windowWidth !== window.innerWidth && window.innerWidth  < 768) {
                    getBounds();
                    windowWidth = window.innerWidth;
                }
            });



            window.yandexMapUpdatePlacemarks = (arrayPlacemarks) => {
                if (ymapsLoaded) {
                    updatePlacemarks(arrayPlacemarks);
                } else {
                    document.addEventListener(`yandexMapObjectLoaded`, () => {
                        updatePlacemarks(arrayPlacemarks);
                    });
                }
            };



            window.yandexMapRemoveAllPlacemarks = () => {
                if (ymapsLoaded) {
                    map.geoObjects.removeAll();
                } else {
                    document.addEventListener(`yandexMapObjectLoaded`, () => {
                        map.geoObjects.removeAll();
                    });
                }
            };






            function init() {
                if (ymapsLoaded || loadStarted) return;

                if (inViewport(wrapper, { offset: 300 })) {
                    loadStarted = true;
                } else {
                    return;
                }

                if (typeof window.yandexMapApiLoadingStarted === `undefined`) {
                    window.yandexMapApiLoadingStarted = true;
                    loadYandexMapApi(closureInitYandexMap);
                } else if (typeof window.yandexMapApiLoaded === `undefined`) {
                    document.addEventListener(`yandexMapApiLoaded`, () => {
                        closureInitYandexMap();
                    });
                } else {
                    closureInitYandexMap();
                }
            }



            function closureInitYandexMap() {
                if (typeof ymaps !== `undefined`) {
                    ymaps.ready(() => {
                        map = new ymaps.Map(jsonContainer.parentElement, $.extend({
                            controls: [`zoomControl`, `trafficControl`, `fullscreenControl`]
                        }, settings.map));

                        map.behaviors.disable(`scrollZoom`);
                        map.controls.remove(`searchControl`);

                        document.body.classList.contains(`mobile`) ? map.behaviors.disable(`drag`) : undefined;

                        settings.placemarks.forEach((item) => {
                            const placemark = new ymaps.Placemark(...item);

                            map.geoObjects.add(placemark);

                            if (buttonsOpeningBalloons) {
                                Array.from(buttonsOpeningBalloons).forEach((self) => {
                                    const btnOpenBalloon = self;

                                    if (btnOpenBalloon.dataset[`id`] === item[1].id) {
                                        btnOpenBalloon.addEventListener(`click`, (event) => {
                                            event.preventDefault();

                                            if (placemark.balloon.isOpen()) {
                                                placemark.balloon.close();
                                            } else {
                                                placemark.balloon.open(undefined, undefined, { autoPan: false });
                                                map.setCenter(placemark.geometry.getCoordinates());
                                            }
                                        });
                                    }
                                });
                            }
                        });

                        !!jsonContainer.dataset[`autocentered`] ? getBounds() : undefined;

                        [`click`, `resize`].forEach((self) => window.addEventListener(self, () => map.container.fitToViewport()));

                        ymapsLoaded = true;

                        if (windowWidth < 768) getBounds();

                        document.dispatchEvent(new CustomEvent(`yandexMapObjectLoaded`));
                    });
                }
            }



            function updatePlacemarks(arrayPlacemarks) {
                yandexMapRemoveAllPlacemarks();

                arrayPlacemarks.forEach(
                    item => map.geoObjects.add(new ymaps.Placemark(...item))
                );

                getBounds();
            }



            function getBounds() {
                if (ymapsLoaded) {
                    map.setBounds(map.geoObjects.getBounds(), {
                        checkZoomRange: true,
                        zoomMargin: 30
                    }).then(() => {
                        if (map.getZoom() > 14) {
                            map.setZoom(14);
                        }
                    });
                }
            }



            function loadYandexMapApi(callbackFunc) {
                window.initYandexMap = callbackFunc;

                scriptjs(`https://api-maps.yandex.ru/2.1/?lang=${scriptLang}`, () => {
                    window.initYandexMap();
                    window.yandexMapApiLoaded = true;
                    document.dispatchEvent(new CustomEvent(`yandexMapApiLoaded`));
                });
            }



        });
    }, 0);
})();