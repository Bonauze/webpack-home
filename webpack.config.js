const fs      = require('fs');
const path    = require('path');
const webpack = require('webpack');



// Plugins - start
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const MiniCssExtractPlugin     = require('mini-css-extract-plugin');
const CleanPlugin              = require('clean-webpack-plugin');
const UglifyJsPlugin           = require('uglifyjs-webpack-plugin');
// Plugins - end



// Flags - start
const env   = process.env.NODE_ENV || 'production';

const flags = {
    browserSync: env === 'browserSync',
    development: env === 'development' || env === 'browserSync',
    production: env === 'production' || env === 'analyzer',
    analyzer: env === 'analyzer'
};
// Flags - end



// Path - start
const bundlePath = path.resolve(__dirname, 'front-end/dist/bundle');
// Path - end



const config = {
    mode: 'none',
    entry: './front-end/src/entry.js',
    output: {
        path: bundlePath,
        pathinfo: !flags.production,
        filename: 'bundle.js',
    },
    resolve: {
        extensions: ['.js']
    },
    module: {
        rules: [
            {
                test: /\.(jpe?g|png|gif|webp)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 10000,
                            name: '[path][name].[ext]'
                        }
                    },
                    {
                        loader: 'img-loader',
                        options: {
                            enabled: flags.production,
                            gifsicle: {
                                interlaced: false
                            },
                            mozjpeg: {
                                quality: 95,
                                progressive: true,
                                arithmetic: false
                            },
                            optipng: false,
                            pngquant: {
                                floyd: 0.5,
                                speed: 2
                            }
                        }
                    }
                ]
            },
            {
                test: /\.svg/,
                use: [
                    {
                        loader: 'svg-url-loader',
                        options: {
                            limit: 10000,
                            name: '[path][name].[ext]'
                        }
                    },
                    {
                        loader: 'img-loader',
                        options: {
                            svgo: {
                                plugins: [
                                    { removeTitle: true },
                                    { removeDimensions: true },
                                    { convertPathData: false }
                                ]
                            }
                        }
                    }
                ]
            },
            {
                test: /(\.(ttf|eot|woff(2)?)|([\/\\]fonts[\/\\].+\.svg))($|\?)/i,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]'
                }
            },
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    getPostCssLoaderConfig()
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    getPostCssLoaderConfig(),
                    'sass-loader'
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    },
                    {
                        loader: 'eslint-loader'
                    }
                ]
            }
        ]
    },
    plugins: [
        new CaseSensitivePathsPlugin(),
        new MiniCssExtractPlugin({
            filename: 'bundle.css',
            allChunks: true
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ],
    devtool : flags.development ? 'source-map' : '',
    stats: {
        children: false
    }
};



if (flags.browserSync) {
    config.watch = true;

    const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

    config.plugins.push(
        new BrowserSyncPlugin({
            host: 'localhost',
            port: 3000,
            server: {
                baseDir: [__dirname]
            },
            directory: true
        })
    );
}



if (flags.development || flags.production) {
    const HtmlWebpackPlugin = require('html-webpack-plugin');

    const namesPugFiles = [];

    fs.readdirSync(__dirname + '/front-end/src/pug/pages').forEach(file => {
        if (file.indexOf('.pug') !== -1) {
            namesPugFiles.push(file.replace('.pug', ''));
        }
    });

    namesPugFiles.forEach((namePugFile) => {
        config.plugins.push(
            new HtmlWebpackPlugin({
                template: path.resolve(__dirname, 'front-end/src/pug/pages/' + namePugFile +'.pug'),
                filename: path.resolve(__dirname, 'front-end/dist/' + namePugFile +'.html'),
                inject: false
            })
        )
    });

    config.module.rules.push({
        test: /\.pug$/,
        loader: 'pug-loader',
        options: {
            pretty: true
        }
    });
}



if (flags.development || flags.production) {
    config.plugins.push(
        new CleanPlugin([bundlePath, path.resolve(__dirname, '../*.html')], {
            root: path.resolve(__dirname),
            verbose: true,
            dry: false
        })
    );
}



if (flags.production) {
    config.plugins.push(
        new UglifyJsPlugin({
            uglifyOptions: {
                warnings: false,
                output: {
                    comments: false
                }
            }
        })
    );
}



if (flags.analyzer) {
    const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

    config.plugins.push(
        new BundleAnalyzerPlugin({
            analyzerMode: 'server',
            analyzerPort: 4000
        })
    );
}



function getPostCssLoaderConfig() {
    const config = {
        loader: 'postcss-loader',
        options: {
            plugins: [
                require('autoprefixer')({
                    browsers: [
                        'last 2 versions',
                        '> 1%'
                    ]
                })
            ]
        }
    };

    if (flags.production) {
        config.options.plugins.push(
            require('cssnano')({
                minifyFontValues: {
                    removeQuotes: false
                },
                discardUnused: {
                    fontFace: false
                },
                zindex: false,
                calc: false,
                reduceIdents: false
            })
        );
    }

    return config;
}

module.exports = config;